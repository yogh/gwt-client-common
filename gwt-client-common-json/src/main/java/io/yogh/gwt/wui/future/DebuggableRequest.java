package io.yogh.gwt.wui.future;

public interface DebuggableRequest {
  void setRequestOrigin(String origin);
}
