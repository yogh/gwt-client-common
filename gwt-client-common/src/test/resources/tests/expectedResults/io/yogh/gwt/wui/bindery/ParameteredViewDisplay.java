package io.yogh.gwt.wui.bindery;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import java.lang.String;

public final class ParameteredViewDisplay {
  private ParameteredViewDisplay() {
  }

  public static final class Default extends ParameteredViewComposite {
    private static final DefaultUiBinder UI_BINDER = GWT.create(DefaultUiBinder.class);

    @Inject
    public Default(@Assisted final String test) {
      super(test);
      initWidget(UI_BINDER.createAndBindUi(this));
    }

    interface DefaultUiBinder extends UiBinder<Widget, Default> {
    }
  }

  public static final class Mobile extends ParameteredViewComposite {
    private static final MobileUiBinder UI_BINDER = GWT.create(MobileUiBinder.class);

    @Inject
    public Mobile(@Assisted final String test) {
      super(test);
      initWidget(UI_BINDER.createAndBindUi(this));
    }

    interface MobileUiBinder extends UiBinder<Widget, Mobile> {
    }
  }
}