package io.yogh.gwt.wui.bindery;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;

public final class AnotherTestViewDisplay {
  private TestViewDisplay() {
  }

  public static final class Default extends AnotherTestViewComposite {
    private static final DefaultUiBinder UI_BINDER = GWT.create(DefaultUiBinder.class);

    public Default() {
      initWidget(UI_BINDER.createAndBindUi(this));
    }

    interface DefaultUiBinder extends UiBinder<Widget, Default> {
    }
  }
}
