/*
 * Copyright 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package io.yogh.gwt;

import java.util.SortedSet;

import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.linker.ConfigurationProperty;
import com.google.gwt.core.ext.linker.PropertyProviderGenerator;
import com.google.gwt.user.rebind.SourceWriter;
import com.google.gwt.user.rebind.StringSourceWriter;
import com.google.gwt.useragent.rebind.UserAgentGenerator;

/**
 * Generator which writes out the JavaScript for determining the value of the <code>user.agent</code> selection property.
 */
public class MobileUserAgentPropertyGenerator implements PropertyProviderGenerator {
  public static String[] MOBILE_USERAGENT_KEYS = new String[] {
      "iPad",
      "iPhone",
      "iPod",
      "Android",
      "Touch"
  };

  /**
   * Writes out the JavaScript function body for determining the value of the <code>user.agent</code> selection property. This method is used to
   * create the selection script and by {@link UserAgentGenerator} to assert at runtime that the correct user agent permutation is executing.
   */
  static void writeUserAgentPropertyJavaScript(final SourceWriter body, final SortedSet<String> possibleValues, final String fallback) {
    // write preamble
    body.println("var userAgent = navigator.userAgent.toLowerCase();");

    for (final String key : MOBILE_USERAGENT_KEYS) {
      // write only selected user agents
      body.println("if ((function() { ");
      body.indentln("return userAgent.indexOf('" + key.toLowerCase() + "') != -1;");
      body.println("})()) return 'mobile';");
    }

    body.println("return 'default';");
  }

  @Override
  public String generate(final TreeLogger logger, final SortedSet<String> possibleValues, final String fallback,
      final SortedSet<ConfigurationProperty> configProperties) {
    final StringSourceWriter body = new StringSourceWriter();
    body.println("{");
    body.indent();
    writeUserAgentPropertyJavaScript(body, possibleValues, fallback);
    body.outdent();
    body.println("}");

    return body.toString();
  }
}
