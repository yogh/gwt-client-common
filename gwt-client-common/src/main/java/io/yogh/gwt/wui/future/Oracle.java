package io.yogh.gwt.wui.future;

import java.util.Map;
import java.util.function.Consumer;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface Oracle<T> {
  boolean ask(final Map<Object, T> cache, final Object key, final Consumer<AsyncCallback<T>> call, final AsyncCallback<T> future);
}
