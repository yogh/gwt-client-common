package io.yogh.gwt.wui.util;

import java.util.Date;

import com.google.gwt.user.client.Cookies;

public class CookieUtil {
  private static final long WEEK = 1000 * 60 * 60 * 24 * 7;

  public static void storeDefaultCookie(final String key, final String value) {
    Cookies.setCookie(key, value, new Date(System.currentTimeMillis() + WEEK));
  }

  public static void getCookie(final String key) {
    Cookies.getCookie(key);
  }

  public static boolean getCookieBoolean(final String key) {
    return "true".equals(Cookies.getCookie(key));
  }
}
