package io.yogh.gwt.wui.main.util;

import com.google.web.bindery.event.shared.EventBus;

import io.yogh.gwt.wui.main.Notification;
import io.yogh.gwt.wui.main.Notification.Type;
import io.yogh.gwt.wui.main.event.NotificationEvent;

/**
 * Sugar for broadcasting Notification objects.
 */
public final class NotificationUtil {
  private NotificationUtil() {
  }

  public static void broadcastMessage(final EventBus eventBus, final String txt) {
    broadcast(eventBus, new Notification(txt));
  }

  /**
   * Broadcasts a message. Text. Not an error.
   *
   * @param eventBus the bus to broadcast over
   * @param txt the message text to broadcast
   * @param url additional url
   */
  public static void broadcastMessage(final EventBus eventBus, final String txt, final String url) {
    broadcast(eventBus, new Notification(txt, url));
  }

  /**
   * Broadcasts an error. Text. Not a message.
   *
   * @param eventBus the bus to broadcast over
   * @param txt the error text to broadcast
   */
  public static void broadcastError(final EventBus eventBus, final String txt) {
    broadcast(eventBus, new Notification(txt, Type.ERROR));
  }

  /**
   * Broadcasts an error. Exception.
   *
   * @param eventBusthe bus to broadcast over
   * @param error the error to broadcast
   */
  public static void broadcastError(final EventBus eventBus, final Throwable error) {
    broadcast(eventBus, new Notification(error));
  }

  /**
   * Broadcasts a warning. Exception.
   *
   * @param eventBus the bus to broadcast over
   * @param error the error to broadcast
   */
  public static void broadcastWarning(final EventBus eventBus, final Throwable error) {
    broadcast(eventBus, new Notification(error, Type.WARNING));
  }

  /**
   * Broadcasts a {@link Notification}.
   *
   * @param eventBus the bus to broadcast over
   * @param notification the object to broadcast
   */
  public static void broadcast(final EventBus eventBus, final Notification notification) {
    broadcast(eventBus, new NotificationEvent(notification));
  }

  private static void broadcast(final EventBus eventBus, final NotificationEvent notification) {
    eventBus.fireEvent(notification);
  }
}
