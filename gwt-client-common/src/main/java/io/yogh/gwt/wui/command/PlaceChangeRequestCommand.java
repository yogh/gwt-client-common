package io.yogh.gwt.wui.command;

import io.yogh.gwt.wui.event.SimpleGenericEvent;
import io.yogh.gwt.wui.place.Place;

public class PlaceChangeRequestCommand extends SimpleGenericEvent<Place> {
  public PlaceChangeRequestCommand(final Place obj) {
    super(obj);
  }
}
