package io.yogh.gwt.wui.activity;

public interface View<P> extends HasPresenter<P> {}
