package io.yogh.gwt.wui.event;

public class RequestClientLoadFailureEvent extends SimpleGenericEvent<Boolean> {
  public RequestClientLoadFailureEvent(final boolean value) {
    super(value);
  }
}
