package io.yogh.gwt.wui.history;

import io.yogh.gwt.wui.place.TokenizedPlace;

public interface PlaceHistoryMapper {
  String getToken(final TokenizedPlace value);

  TokenizedPlace getPlace(String token);
}
