package io.yogh.gwt.wui.util;

import java.util.List;
import java.util.function.Consumer;

import com.google.web.bindery.event.shared.EventBus;

import io.yogh.gwt.wui.activators.IsActivatorInfo;

public class DummyAssistant implements ReplacementAssistant, ActivatorAssistant, ObservingReplacementAssistant {

  @Override
  public void setEventBus(final EventBus eventBus) {
    // No-op
  }

  @Override
  public String replace(final String origin) {
    return origin;
  }

  @Override
  public String replaceStrict(final String origin) {
    if (origin.indexOf("{") > -1) {
      throw new RuntimeException("Can not dummily replace a strict tag: " + origin);
    }

    return origin;
  }

  @Override
  public <T extends IsActivatorInfo> ReplacementRegistration register(final List<T> activators, final Consumer<List<T>> consumer) {
    return () -> {};
  }

  @Override
  public ReplacementRegistration register(final String template, final Consumer<String> onChange) {
    return () -> {};
  }

  @Override
  public ReplacementRegistration registerStrict(final String template, final Consumer<String> onChange) {
    return () -> {};
  }
}
