package io.yogh.gwt.wui.util;

import com.google.inject.ImplementedBy;

import io.yogh.gwt.wui.widget.HasEventBus;

@ImplementedBy(DummyAssistant.class)
public interface ReplacementAssistant extends HasEventBus {
  String replace(String origin);

  /**
   * Replace the given string, and throw an error when there are tags that cannot be replaced.
   */
  String replaceStrict(String origin);
}
