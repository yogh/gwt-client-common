package io.yogh.gwt.wui.event;

public class RequestConnectionFailureEvent extends SimpleGenericEvent<Boolean> {
  public RequestConnectionFailureEvent(final boolean value) {
    super(value);
  }
}
