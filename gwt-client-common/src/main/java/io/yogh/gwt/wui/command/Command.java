package io.yogh.gwt.wui.command;

import com.google.web.bindery.event.shared.Event;

public interface Command<H extends Event<?>> {
  void setSilent(boolean silent);

  /**
   * Short-hand for setSilent(true), can be used to silence this command's event.
   *
   * Silencing is useful for example when the command has no effect, or when the functionality is duplicated elsewhere.
   */
  default void silence() {
    setSilent(true);
  }

  boolean isSilent();

  H getEvent();
}
