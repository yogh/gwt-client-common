package io.yogh.gwt.wui.util;

import java.util.function.Function;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestBuilder.Method;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class LegacyRequestUtil {
  private static final int CLIENT_TIMEOUT = 30000;

  public static <T> void doGet(final String url, final Function<AsyncCallback<T>, RequestCallback> parser, final AsyncCallback<T> callback) {
    doGet(url, parser.apply(callback));
  }

  public static <T> void doGet(final String url, final AsyncCallback<Response> callback) {
    doGet(url, createRequestCallback(callback));
  }

  public static <T> void doPost(final String url, final String payload, final Function<AsyncCallback<T>, RequestCallback> parser,
      final AsyncCallback<T> callback) {
    doPost(url, payload, parser.apply(callback));
  }

  public static <T> void doPost(final String url, final String payload, final AsyncCallback<Response> callback) {
    doPost(url, payload, createRequestCallback(callback));
  }

  public static void doGet(final String url, final RequestCallback callback) {
    doRequest(RequestBuilder.GET, url, callback);
  }

  public static <T> void doPost(final String url, final Function<AsyncCallback<T>, RequestCallback> parser, final AsyncCallback<T> callback) {
    doPost(url, null, parser.apply(callback));
  }

  public static void doPost(final String url, final String payload, final RequestCallback callback) {
    doRequest(RequestBuilder.POST, url, payload, callback);
  }

  public static <T> void doRemove(final String url, final Function<AsyncCallback<T>, RequestCallback> parser, final AsyncCallback<T> callback) {
    doRequest(RequestBuilder.DELETE, url, parser.apply(callback));
  }

  private static void doRequest(final Method method, final String url, final RequestCallback callback) {
    doRequest(method, url, null, callback);
  }

  private static void doRequest(final Method method, final String url, final String payload, final RequestCallback callback) {
    final RequestBuilder builder = new RequestBuilder(method, url);

    builder.setTimeoutMillis(CLIENT_TIMEOUT);
    builder.setHeader("content-type", "application/json");

    if (payload != null) {
      builder.setRequestData(payload);
    }

    if (callback != null) {
      builder.setCallback(callback);
    }

    try {
      builder.send();
    } catch (final RequestException e) {
      throw new RuntimeException(e);
    }
  }

  private static RequestCallback createRequestCallback(final AsyncCallback<Response> callback) {
    return new RequestCallback() {
      @Override
      public void onResponseReceived(final Request request, final Response response) {
        callback.onSuccess(response);
      }

      @Override
      public void onError(final Request request, final Throwable exception) {
        callback.onFailure(exception);
      }
    };
  }
}
