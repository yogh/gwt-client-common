package io.yogh.gwt.wui.service;

import org.apache.http.HttpStatus;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class JsonAsyncCallback extends JsonErrorResponseCallback implements RequestCallback {
  private final AsyncCallback<JSONValue> callback;

  private JsonAsyncCallback(final AsyncCallback<JSONValue> callback) {
    this.callback = callback;
  }

  @Override
  public void onResponseReceived(final Request request, final Response response) {
    if (response.getStatusCode() == HttpStatus.SC_OK) {
      final JSONValue parsed = JSONParser.parseStrict(response.getText());
      try {
        callback.onSuccess(parsed);
      } catch (final Exception e) {
        callback.onFailure(e);
      }
    } else {
      callback.onFailure(parseError(response));
    }
  }

  @Override
  public void onError(final Request request, final Throwable exception) {
    callback.onFailure(exception);
  }

  public static RequestCallback create(final AsyncCallback<JSONValue> callback) {
    return new JsonAsyncCallback(callback);
  }
}
