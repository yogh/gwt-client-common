package io.yogh.gwt.wui.service;

import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.rpc.AsyncCallback;

import io.yogh.gwt.wui.future.JsonParserCallback;
import io.yogh.gwt.wui.future.JsonRequestCallback;
import io.yogh.gwt.wui.service.json.JSONObjectHandle;

public final class BooleanResultJsonParser {
  public static final class LegacyCallback extends JsonParserCallback<Boolean> {
    public LegacyCallback(final AsyncCallback<Boolean> callback) {
      super(callback);
    }

    @Override
    public Boolean parse(final JSONValue result) {
      return BooleanResultJsonParser.parse(result);
    }
  }

  public static final class ElementalCallback extends ForwardedAsyncCallback<Boolean, String> {
    public ElementalCallback(final AsyncCallback<Boolean> callback) {
      super(callback);
    }

    @Override
    public Boolean convert(final String value) {
      return BooleanResultJsonParser.parse(value);
    }
  }

  public static final Boolean parse(final String text) {
    return parse(JSONObjectHandle.fromText(text));
  }

  public static final Boolean parse(final JSONValue json) {
    return parse(JSONObjectHandle.fromJson(json));
  }

  public static final Boolean parse(final JSONObjectHandle result) {
    return result.getBoolean("result");
  }

  public static AsyncCallback<String> wrap(final AsyncCallback<Boolean> callback) {
    return new ElementalCallback(callback);
  }

  public static RequestCallback wrapLegacy(final AsyncCallback<Boolean> callback) {
    return JsonRequestCallback.create(new LegacyCallback(callback));
  }
}
