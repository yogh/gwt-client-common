package io.yogh.gwt.wui.util;

import java.util.function.Consumer;

import com.google.inject.ImplementedBy;

@ImplementedBy(DummyAssistant.class)
public interface ObservingReplacementAssistant {
  ReplacementRegistration register(String template, Consumer<String> onChange);

  ReplacementRegistration registerStrict(String template, Consumer<String> onChange);
}
