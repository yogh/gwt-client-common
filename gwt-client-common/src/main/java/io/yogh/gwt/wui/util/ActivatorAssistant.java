package io.yogh.gwt.wui.util;

import java.util.List;
import java.util.function.Consumer;

import com.google.inject.ImplementedBy;

import io.yogh.gwt.wui.activators.IsActivatorInfo;
import io.yogh.gwt.wui.widget.HasEventBus;

@ImplementedBy(DummyAssistant.class)
public interface ActivatorAssistant extends HasEventBus {
  <T extends IsActivatorInfo> ReplacementRegistration register(List<T> activators, Consumer<List<T>> consumer);
}
