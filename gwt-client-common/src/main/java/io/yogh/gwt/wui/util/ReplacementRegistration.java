package io.yogh.gwt.wui.util;

public interface ReplacementRegistration {
  void unregister();
}