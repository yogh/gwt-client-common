package io.yogh.gwt.wui.command;

import com.google.web.bindery.event.shared.binder.GenericEvent;

public abstract class SimpleCommand<E extends GenericEvent> extends GenericEvent implements Command<E> {
  private E event;
  private boolean silent;

  public SimpleCommand() {}

  @Override
  public E getEvent() {
    return event == null ? createEvent() : event;
  }

  @Override
  public void setSilent(final boolean silent) {
    this.silent = silent;
  }

  @Override
  public boolean isSilent() {
    return silent;
  }

  protected abstract E createEvent();
}
