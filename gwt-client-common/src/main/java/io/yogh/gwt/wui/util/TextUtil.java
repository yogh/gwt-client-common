package io.yogh.gwt.wui.util;

import java.util.Iterator;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayString;

public final class TextUtil {
  private TextUtil() {}

  public interface TextFactory<T> {
    String getText(T item);
  }

  /* Text utils */

  /**
   * Capitalises the first letter in the given string.
   *
   * @param nocaps The string to capitalise.
   * @return the given argument if the argument is null or empty, or a string with the first character uppercased.
   */
  public static String capitalise(final String nocaps) {
    if (nocaps == null || nocaps.isEmpty()) {
      return nocaps;
    }

    return new StringBuilder(nocaps.substring(0, 1).toUpperCase()).append(nocaps.substring(1)).toString();
  }

  public static String formatCurrency(final double value) {
    return String.valueOf("€ " + toFixed(value, 2));
  }

  public static native String toFixed(double d, int decimals) /*-{
                                                              return d.toFixed(decimals).toLocaleString();
                                                              }-*/;

  public static <T> String joinItems(final Iterator<T> iterator, final TextFactory<T> textMachine, final String seperator) {
    final JsArrayString jsa = JavaScriptObject.createArray().cast();

    while (iterator.hasNext()) {
      jsa.push(textMachine.getText(iterator.next()));
    }

    return jsa.join(seperator);
  }

  public static String joinStrings(final String... strings) {
    return joinStrings(strings, " ");
  }

  public static String joinStrings(final String[] strings, final String seperator) {
    final JsArrayString jsa = JavaScriptObject.createArray().cast();

    for (final String string : strings) {
      jsa.push(string);
    }

    return jsa.join(seperator);
  }

  public static String formatString(final String format, final Object... params) {
    String formatted = format;
    for (final Object param : params) {
      formatted = formatted.replaceFirst("%s", String.valueOf(param));
    }

    return formatted;
  }
}
