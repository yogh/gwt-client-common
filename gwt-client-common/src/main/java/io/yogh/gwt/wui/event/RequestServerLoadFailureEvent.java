package io.yogh.gwt.wui.event;

public class RequestServerLoadFailureEvent extends SimpleGenericEvent<Boolean> {
  public RequestServerLoadFailureEvent(final boolean value) {
    super(value);
  }
}
