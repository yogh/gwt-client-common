package io.yogh.gwt.wui.history;

import com.google.inject.ImplementedBy;

import io.yogh.gwt.wui.place.TokenizedPlace;

@ImplementedBy(HistoryManagerImpl.class)
public interface HistoryManager {
  void handleCurrentHistory();

  TokenizedPlace getPlace(String token);
}
