package io.yogh.gwt.wui.event;

import io.yogh.gwt.wui.place.Place;

public class PlaceChangeEvent extends SimpleGenericEvent<Place> {
  private final boolean silent;

  public PlaceChangeEvent(final Place obj) {
    this(obj, true);
  }

  public PlaceChangeEvent(final Place obj, final boolean silent) {
    super(obj);

    this.silent = silent;
  }

  public boolean isSilent() {
    return silent;
  }
}
