package io.yogh.gwt.wui.exception;

public class SneakerException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public SneakerException(final Throwable cause) {
    super(cause);
  }
}
