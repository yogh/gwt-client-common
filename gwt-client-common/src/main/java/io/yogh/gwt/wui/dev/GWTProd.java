package io.yogh.gwt.wui.dev;

public final class GWTProd {
  private static boolean dev = true;
  
  private GWTProd() {}

  public static void setIsDev(final boolean dev) {
    GWTProd.dev = dev;
  }

  /**
   * Logs a message to the console. Calls are _not_ optimized out in Production Mode.
   */
  public static native void log(Object message) /*-{
    console.log(message );
  }-*/;

  /**
   * Logs a message to the warn console. Calls are _not_ optimized out in Production Mode.
   */
  public static native void warn(Object message) /*-{
    console.warn(message );
  }-*/;

  /**
   * Logs a message to the warn console. Calls are _not_ optimized out in Production Mode.
   */
  public static native void error(Object message) /*-{
    console.error(message );
  }-*/;

  public static void info(final String string) {
    log("INFO", string);
  }

  public static void error(final String marker, final String string) {
    error("[" + marker + "] " + string);
  }

  public static void warn(final String marker, final String string) {
    warn("[" + marker + "] " + string);
  }

  public static void log(final String marker, final String string) {
    log("[" + marker + "] " + string);
  }

  public static boolean isDev() {
    return dev;
  }
}
