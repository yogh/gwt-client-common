package io.yogh.gwt.wui.command;

import com.google.web.bindery.event.shared.binder.GenericEvent;

/**
 * And event masquerading as a command.
 *
 * This command is to be treated as an event, since its invocation is atomically inconsequential to any state.
 *
 * Exists solely to semantically make sense.
 */
public class MasqueradingCommand extends GenericEvent {}
