package io.yogh.gwt.wui.command;

import io.yogh.gwt.wui.event.PlaceChangeEvent;
import io.yogh.gwt.wui.place.Place;

public class PlaceChangeCommand extends SimpleGenericCommand<Place, PlaceChangeEvent> {
  private boolean cancelled;

  private Place redirect;

  public PlaceChangeCommand(final Place obj) {
    this(obj, true);
  }

  public PlaceChangeCommand(final Place obj, final boolean silent) {
    super(obj);
  }

  @Override
  protected PlaceChangeEvent createEvent(final Place value) {
    return new PlaceChangeEvent(value);
  }

  public boolean isCancelled() {
    return cancelled;
  }

  public void setCancelled(final boolean cancelled) {
    this.cancelled = cancelled;
  }

  public void cancel() {
    setCancelled(true);
  }

  public void setRedirect(final Place redirect) {
    this.redirect = redirect;
    silence();
  }

  public Place getRedirect() {
    return redirect;
  }

  public boolean isRedirected() {
    return redirect != null;
  }
}
