package io.yogh.gwt.wui.service;

import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;

import io.yogh.gwt.wui.service.json.JSONObjectHandle;

public class JsonErrorResponseCallback {
  protected Throwable parseError(final Response response) {
    final JSONValue parsed = JSONParser.parseStrict(response.getText());

    final JSONObjectHandle obj = new JSONObjectHandle(parsed.isObject());

    return new RuntimeException(obj.getString("message"));
  }
}
