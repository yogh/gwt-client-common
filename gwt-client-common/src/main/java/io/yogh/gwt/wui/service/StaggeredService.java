package io.yogh.gwt.wui.service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.rpc.AsyncCallback;

import io.yogh.gwt.wui.util.LegacyRequestUtil;

public abstract class StaggeredService {
  private boolean busy;
  private final List<Runnable> straglers = new ArrayList<>();

  protected <T> void request(final String url, final Function<AsyncCallback<T>, RequestCallback> parser, final AsyncCallback<T> callback) {
    straglers.add(() -> LegacyRequestUtil.doGet(url, parser, ProxiedCallback.wrap(callback, () -> requestNext())));

    if (!busy) {
      busy = true;
      requestNext();
    }
  }

  protected void request(final String url, final AsyncCallback<Response> callback) {
    straglers.add(() -> LegacyRequestUtil.doGet(url, ProxiedCallback.wrap(callback, () -> requestNext())));

    if (!busy) {
      busy = true;
      requestNext();
    }
  }

  protected <T, C> void request(final String url, final T payload, final Function<T, String> serializer,
      final Function<AsyncCallback<C>, RequestCallback> parser, final AsyncCallback<C> callback) {
    request(url, serializer.apply(payload), parser, callback);
  }

  protected <T, C> void request(final String url, final String payload, final Function<AsyncCallback<C>, RequestCallback> parser,
      final AsyncCallback<C> callback) {
    straglers.add(() -> LegacyRequestUtil.doPost(url, payload, parser, ProxiedCallback.wrap(callback, () -> requestNext())));

    if (!busy) {
      busy = true;
      requestNext();
    }
  }

  protected <T> void request(final String url, final T payload, final Function<T, String> serializer, final AsyncCallback<Response> callback) {
    request(url, serializer.apply(payload), callback);
  }

  protected void request(final String url, final String payload, final AsyncCallback<Response> callback) {
    straglers.add(() -> LegacyRequestUtil.doPost(url, payload, ProxiedCallback.wrap(callback, () -> requestNext())));

    if (!busy) {
      busy = true;
      requestNext();
    }
  }

  private void requestNext() {
    if (straglers.isEmpty()) {
      busy = false;
    } else {
      straglers.remove(0).run();
    }
  }
}
