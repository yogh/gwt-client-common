package io.yogh.gwt.wui.dev;

import io.yogh.gwt.wui.widget.HasEventBus;

public interface DevelopmentObserver extends HasEventBus {}
