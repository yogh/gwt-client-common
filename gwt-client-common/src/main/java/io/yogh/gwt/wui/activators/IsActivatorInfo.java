package io.yogh.gwt.wui.activators;

public interface IsActivatorInfo {
  String name();
}
