package io.yogh.gwt.wui.service;

import org.apache.http.HttpStatus;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.rpc.AsyncCallback;

public final class VoidParser extends JsonErrorResponseCallback implements RequestCallback {
  private final AsyncCallback<Void> callback;

  public VoidParser(final AsyncCallback<Void> callback) {
    this.callback = callback;
  }

  @Override
  public void onResponseReceived(final Request request, final Response response) {
    if (response.getStatusCode() == HttpStatus.SC_OK) {
      try {
        callback.onSuccess(null);
      } catch (final Exception e) {
        callback.onFailure(e);
      }
    } else {
      callback.onFailure(parseError(response));
    }
  }

  @Override
  public void onError(final Request request, final Throwable exception) {
    GWT.log("FAILURE and THROWING! 3");
    callback.onFailure(exception);
  }

  public static RequestCallback wrap(final AsyncCallback<Void> callback) {
    return new VoidParser(callback);
  }
}
