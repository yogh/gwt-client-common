package io.yogh.gwt.wui.activity;

public interface BaseActivity<C> {
  void onStart();
  
  void onStart(C container);
}
