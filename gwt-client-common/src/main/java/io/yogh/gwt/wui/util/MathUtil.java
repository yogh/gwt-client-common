package io.yogh.gwt.wui.util;

/**
 * Math class specific for GWT.
 */
public final class MathUtil {

  private static final double ROUND_UP = 0.5;

  private MathUtil() {
    // util class.
  }

  /**
   * Java uses the if-you-ask-me-incorrect-and-dumb-version of %, which yields a negative output for negative input.
   *
   * Ie. -3 % 2 yields -1, while it should yield 1. This is a correct implementation.
   *
   * @param a
   *          Input
   * @param b
   *          Mod
   * @return Positive output
   */
  public static int positiveMod(final int a, final int b) {
    final int mod = a % b;
    return mod < 0 ? mod + b : mod;
  }

  /**
   * Round a double to an int. Not using standard Math.round because that returns a long and long is not nicely supported in JavaScript.
   * @param value value to round
   * @return value rounded to int
   */
  public static int round(final double value) {
    return (int) (value + (value < 0 ? -ROUND_UP : ROUND_UP));
  }
}