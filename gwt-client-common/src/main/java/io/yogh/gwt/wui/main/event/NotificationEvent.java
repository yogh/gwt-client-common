package io.yogh.gwt.wui.main.event;

import io.yogh.gwt.wui.event.SimpleGenericEvent;
import io.yogh.gwt.wui.main.Notification;

/**
 * Event which is fired when a notification is appropriate.
 */
public class NotificationEvent extends SimpleGenericEvent<Notification> {
  /**
   * @param obj the {@link Notification} to encapsulate
   */
  public NotificationEvent(final Notification obj) {
    super(obj);
  }
}
