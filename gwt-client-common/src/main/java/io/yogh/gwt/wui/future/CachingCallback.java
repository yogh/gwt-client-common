package io.yogh.gwt.wui.future;

import java.util.Map;

import com.google.gwt.user.client.rpc.AsyncCallback;

public class CachingCallback<T> implements AsyncCallback<T> {
  private final Map<Object, T> cache;
  private final AsyncCallback<T> callback;
  private final Object key;

  public CachingCallback(final Object key, final Map<Object, T> cache, final AsyncCallback<T> callback) {
    this.key = key;
    this.cache = cache;
    this.callback = callback;
  }

  @Override
  public void onSuccess(final T result) {
    cache.put(key, result);
    callback.onSuccess(result);
  }

  @Override
  public void onFailure(final Throwable caught) {
    // Fall through without caching
    callback.onFailure(caught);
  }
}
