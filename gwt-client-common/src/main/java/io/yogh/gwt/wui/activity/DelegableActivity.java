package io.yogh.gwt.wui.activity;

import com.google.web.bindery.event.shared.EventBus;

import io.yogh.gwt.wui.command.PlaceChangeCommand;
import io.yogh.gwt.wui.place.Place;

public interface DelegableActivity {
  default boolean delegate(final EventBus eventBus, final PlaceChangeCommand c) {
    return false;
  }

  default boolean isDelegable(final Place place) {
    return false;
  }
}
