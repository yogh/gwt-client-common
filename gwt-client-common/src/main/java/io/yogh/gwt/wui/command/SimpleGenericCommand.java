package io.yogh.gwt.wui.command;

import io.yogh.gwt.wui.event.SimpleGenericEvent;

public abstract class SimpleGenericCommand<T, E extends SimpleGenericEvent<T>> extends SimpleGenericEvent<T> implements Command<E> {
  private boolean silent;

  private E event;

  public SimpleGenericCommand() {}

  public SimpleGenericCommand(final T value) {
    super(value);
  }

  public SimpleGenericCommand(final T value, final boolean silent) {
    super(value);

    this.silent = silent;
  }

  @Override
  public void setValue(final T value) {
    super.setValue(value);
    this.event = createEvent(value);
  }

  protected abstract E createEvent(T value);

  @Override
  public E getEvent() {
    return event == null ? createEvent(getValue()) : event;
  }

  @Override
  public boolean isSilent() {
    return silent;
  }

  @Override
  public void setSilent(final boolean silent) {
    this.silent = silent;
  }
}
