package io.yogh.gwt.wui.event;

import java.util.ArrayList;
import java.util.List;

import com.google.web.bindery.event.shared.HandlerRegistration;

public class BundledRegistration {
  private final List<HandlerRegistration> registrations = new ArrayList<>();

  public void add(final HandlerRegistration registration) {
    registrations.add(registration);
  }

  public void retire() {
    registrations.forEach(HandlerRegistration::removeHandler);
    registrations.clear();
  }
}
