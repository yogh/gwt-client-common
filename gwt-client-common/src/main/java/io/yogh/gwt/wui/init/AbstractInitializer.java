package io.yogh.gwt.wui.init;

import java.util.function.Consumer;

import io.yogh.gwt.wui.dev.GWTProd;
import io.yogh.gwt.wui.util.GWTAtomicInteger;

public abstract class AbstractInitializer implements Initializer {
  private final GWTAtomicInteger counter = new GWTAtomicInteger();

  private Runnable complete;

  private final int completionThreshold;

  private Consumer<Throwable> fail;

  public AbstractInitializer(final int completionThreshold) {
    this.completionThreshold = completionThreshold;
  }

  @Override
  public void init(final Runnable complete, final Consumer<Throwable> fail) {
    this.complete = complete;
    this.fail = fail;
    init();
  }

  public abstract void init();

  protected void complete() {
    if (counter.incrementAndGet() == completionThreshold) {
      complete.run();
    }
  }

  protected void completeWithWarning(final String message, final Throwable e) {
    complete();

    GWTProd.warn("Failure while initialising application: " + message);
  }

  protected void fail(final String message, final Throwable e) {
    GWTProd.error("Unrecoverable failure while initialising application: " + message + " > " + e.getMessage());

    fail.accept(new Exception(message));
  }
}
