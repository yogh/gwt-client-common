package io.yogh.gwt.wui.daemon;

import io.yogh.gwt.wui.widget.HasEventBus;

public interface Daemon extends HasEventBus {
  default void init() {}
}
