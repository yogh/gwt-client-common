package io.yogh.gwt.wui.activity;

import com.google.inject.ImplementedBy;

@ImplementedBy(WidgetActivityManager.class)
public interface ActivityManager<C> {
  void setPanel(C panel);
}
