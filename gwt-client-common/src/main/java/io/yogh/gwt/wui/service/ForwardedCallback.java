package io.yogh.gwt.wui.service;

import java.util.function.Function;

import com.google.gwt.user.client.rpc.AsyncCallback;

public class ForwardedCallback<F, C> implements AsyncCallback<C> {
  protected AsyncCallback<F> callback;
  private Function<C, F> mapper;

  public ForwardedCallback(final AsyncCallback<F> callback) {
    this.callback = callback;
  }

  public static <F, C> ForwardedCallback<F, C> wrap(final AsyncCallback<F> callback) {
    return new ForwardedCallback<F, C>(callback);
  }

  public ForwardedCallback<F, C> map(final Function<C, F> mapper) {
    this.mapper = mapper;
    return this;
  }

  @Override
  public void onSuccess(final C result) {
    if (mapper != null) {
      callback.onSuccess(mapper.apply(result));
    }
  }

  @Override
  public void onFailure(final Throwable caught) {
    callback.onFailure(caught);
  }
}
