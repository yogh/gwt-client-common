package io.yogh.gwt.wui.daemon;

import io.yogh.gwt.wui.widget.HasEventBus;

public interface DaemonBootstrapper extends HasEventBus, Daemon {}
