package io.yogh.gwt.wui.util;

import io.yogh.gwt.wui.dev.GWTProd;

public class WebUtil {
  private static String absoluteRoot;

  private WebUtil() {}

  public static String getAbsoluteRoot() {
    return absoluteRoot;
  }

  public static void setAbsoluteRoot(final String absoluteRoot) {
    final String actual = sanitize(absoluteRoot);

    GWTProd.info("Root path set to: " + actual);

    WebUtil.absoluteRoot = actual;
  }

  private static String sanitize(final String absoluteRoot) {
    return "/" + absoluteRoot.replace("http://", "").replaceAll("https://", "").split("/", 2)[1];
  }
}