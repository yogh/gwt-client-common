package io.yogh.gwt.wui.command;

import io.yogh.gwt.wui.event.SimpleGenericEvent;

/**
 * @param <T> The value to transform [Transform]
 * @param <M> The transformed value [Morphed]
 * @param <E> The event containing the transformed/morphed value [Event]
 */
public abstract class ProxyCommand<T, M, E extends SimpleGenericEvent<M>> extends SimpleGenericEvent<T> implements Command<E> {
  private boolean silent;

  private E event;

  public ProxyCommand() {}

  public ProxyCommand(final T value) {
    super(value);
  }

  public ProxyCommand(final T value, final boolean silent) {
    super(value);

    this.silent = silent;
  }

  @Override
  public E getEvent() {
    if (event == null) {
      throw new RuntimeException(
          "Proxy command (" + getClass().getSimpleName() + ") was not handled while this is mandatory; Command's Event is not set.");
    }

    return event;
  }

  public void setEvent(final E event) {
    this.event = event;
  }

  @Override
  public boolean isSilent() {
    return silent;
  }

  @Override
  public void setSilent(final boolean silent) {
    this.silent = silent;
  }
}
