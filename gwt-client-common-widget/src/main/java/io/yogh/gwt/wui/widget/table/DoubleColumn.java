package io.yogh.gwt.wui.widget.table;

import java.util.function.Function;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class DoubleColumn<L> extends SimpleWidgetFactory<L> {
  private static final String NOT_AVAILABLE_VALUE = "-";

  private int decimals;

  private Function<L, Double> formatter;

  public DoubleColumn() {}

  public DoubleColumn(final Function<L, Double> formatter) {
    this(formatter, 0);
  }

  public DoubleColumn(final int decimals) {
    this.decimals = decimals;
  }

  public DoubleColumn(final Function<L, Double> formatter, final int decimals) {
    this.formatter = formatter;
    this.decimals = decimals;
  }

  @Override
  public Widget createWidget(final L object) {
    return new Label(formatValue(getValue(object)));
  }

  protected String displayValue(final Double value) {
    if (value == null) {
      return NOT_AVAILABLE_VALUE;
    }

    return formatValue(value);
  }

  protected String formatValue(final Double value) {
    return String.valueOf(roundValue(value));
  }

  protected String roundValue(final Double value) {
    return toFixed(value, decimals);
  }

  public static native String toFixed(double d, int decimals) /*-{
                                                              return d.toFixed(decimals);
                                                              }-*/;

  public Double getValue(final L object) {
    return formatter.apply(object);
  }
}
