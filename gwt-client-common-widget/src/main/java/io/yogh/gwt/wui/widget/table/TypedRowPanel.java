package io.yogh.gwt.wui.widget.table;

import com.google.gwt.user.client.ui.IsWidget;

public interface TypedRowPanel<R extends DivTableRow> extends IsWidget {
  int size();

  R getWidget(int i);

  default boolean isEmpty() {
    return size() == 0;
  }

  void insert(R row, int index);

  void clear();
}
