package io.yogh.gwt.wui.widget.table;

import java.util.function.Consumer;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class SimpleWrappingDivTableRow implements DivTableRow {
  protected final SimplePanel container;
  private final FlowPanel panel;

  public SimpleWrappingDivTableRow() {
    this(new FlowPanel());
  }

  public SimpleWrappingDivTableRow(final Consumer<Widget> decorator) {
    this(new FlowPanel(), decorator);
  }

  public SimpleWrappingDivTableRow(final FlowPanel panel) {
    this(panel, null);
  }

  public SimpleWrappingDivTableRow(final FlowPanel panel, final Consumer<Widget> decorator) {
    this.panel = panel;

    container = new SimplePanel();
    if (decorator != null) {
      decorator.accept(container);
    }

    container.setWidget(panel);
  }

  @Override
  public Widget asWidget() {
    return container;
  }

  @Override
  public Widget asStyleWidget() {
    return panel;
  }

  @Override
  public HasWidgets asHasWidgets() {
    return panel;
  }
}
