package io.yogh.gwt.wui.widget;

public class LazyRunnable implements Runnable {
  private Runnable lazy;

  @Override
  public void run() {
    lazy.run();
  }

  public void setLazy(final Runnable lazy) {
    this.lazy = lazy;
  }
}