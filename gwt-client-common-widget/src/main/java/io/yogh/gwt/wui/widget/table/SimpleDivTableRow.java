package io.yogh.gwt.wui.widget.table;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasWidgets;

public class SimpleDivTableRow extends FlowPanel implements DivTableRow {
  @Override
  public HasWidgets asHasWidgets() {
    return this;
  }
}
