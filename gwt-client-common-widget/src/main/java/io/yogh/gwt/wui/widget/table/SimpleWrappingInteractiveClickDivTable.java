package io.yogh.gwt.wui.widget.table;

public class SimpleWrappingInteractiveClickDivTable<T> extends InteractiveClickDivTable<T, SimpleWrappingInteractiveDivTableRow> {
  /**
   * Create a simple default DivTable.
   */
  public SimpleWrappingInteractiveClickDivTable() {
    this(false);
  }

  /**
   * Creates a DivTable that's loading by default (or not).
   *
   * @param loadingByDefault Whether this widget shows 'loading' content if empty.
   */
  public SimpleWrappingInteractiveClickDivTable(final boolean loadingByDefault) {
    this(new TypedFlowPanel<SimpleWrappingInteractiveDivTableRow>(), loadingByDefault);
  }

  /**
   * Create a DivTable with a custom row panel.
   *
   * @param rowPanel Custom row panel to use for rows.
   */
  public SimpleWrappingInteractiveClickDivTable(final TypedRowPanel<SimpleWrappingInteractiveDivTableRow> rowPanel) {
    this(rowPanel, false);
  }

  /**
   * Creates a DivTable that's loading by default (or not).
   *
   * @param rowPanel         Custom row panel to use for rows.
   * @param loadingByDefault Whether this widget shows 'loading' content if empty.
   */
  public SimpleWrappingInteractiveClickDivTable(final TypedRowPanel<SimpleWrappingInteractiveDivTableRow> rowPanel, final boolean loadingByDefault) {
    super(rowPanel, loadingByDefault);

    installRowCreator(v -> new SimpleWrappingInteractiveDivTableRow());
  }
}
