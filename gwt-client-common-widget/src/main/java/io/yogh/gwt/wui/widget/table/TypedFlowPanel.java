package io.yogh.gwt.wui.widget.table;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;

public class TypedFlowPanel<R extends DivTableRow> implements IsWidget, TypedRowPanel<R> {
  private final FlowPanel panel = new FlowPanel();

  public void add(final R child) {
    panel.add(child);
  }

  @Override
  public void insert(final R w, final int beforeIndex) {
    panel.insert(w, beforeIndex);
  }

  @Override
  @SuppressWarnings("unchecked")
  public R getWidget(final int index) {
    return (R) panel.getWidget(index);
  }

  @Override
  public int size() {
    return panel.getWidgetCount();
  }

  @Override
  public FlowPanel asWidget() {
    return panel;
  }

  @Override
  public void clear() {
    panel.clear();
  }

  @Override
  public boolean isEmpty() {
    return panel.getWidgetCount() == 0;
  }

  public boolean remove(final int i) {
    return panel.remove(i);
  }
}
