package io.yogh.gwt.wui.widget.table;

import java.util.function.Function;

import com.google.gwt.user.client.ui.TextBox;

public class EditableTextColumn<L> extends WidgetFactory<L, TextBox> {
  private final boolean displayTitle;
  private Function<L, String> formatter;

  public EditableTextColumn() {
    this(false);
  }

  public EditableTextColumn(final Function<L, String> formatter) {
    this(formatter, false);
  }

  public EditableTextColumn(final boolean displayTitle) {
    this.displayTitle = displayTitle;
  }

  public EditableTextColumn(final Function<L, String> formatter, final boolean displayTitle) {
    this.formatter = formatter;
    this.displayTitle = displayTitle;
  }

  @Override
  public TextBox createWidget(final L object) {
    final TextBox box = new TextBox();

    final String value = getValue(object);

    if (value != null) {
      box.setText(value);
    }

    if (displayTitle) {
      final String titleText = getTitleText(object);
      box.setTitle(titleText == null ? value : titleText);
    }

    box.addValueChangeHandler(v -> setValue(object, v.getValue()));

    return box;
  }

  protected void setValue(final L object, final String value) {}

  public String getTitleText(final L object) {
    return null;
  }

  public String getValue(final L object) {
    return formatter.apply(object);
  }
}

