package io.yogh.gwt.wui.widget.table;

import java.util.Collection;
import java.util.function.Function;

/**
 * Defines a column that is an inner table.
 *
 * @param <I> Inner content type
 * @param <O> Outer content (row) type
 * @param <T> Type of the table
 */
public class TableColumn<I, O, T extends IsDataTable<I>> extends WidgetFactory<O, T> {
  private Function<O, Collection<I>> retriever;
  private Function<O, T> creator;

  public TableColumn() {}

  public TableColumn(final Function<O, Collection<I>> retriever, final Function<O, T> creator) {
    installRetriever(retriever);
    installCreator(creator);
  }

  @Override
  public T createWidget(final O object) {
    final T table = createDataTable(object);

    if (table != null) {
      table.asDataTable().setRowData(getRowData(object));
    }

    return table;
  }

  public Collection<I> getRowData(final O object) {
    return retriever.apply(object);
  }

  public T createDataTable(final O object) {
    return creator.apply(object);
  }

  public void installRetriever(final Function<O, Collection<I>> retriever) {
    this.retriever = retriever;
  }

  public void installCreator(final Function<O, T> creator) {
    this.creator = creator;
  }
}
