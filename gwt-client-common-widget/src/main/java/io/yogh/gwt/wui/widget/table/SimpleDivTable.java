package io.yogh.gwt.wui.widget.table;

public class SimpleDivTable<T> extends DivTable<T, SimpleDivTableRow> {
  /**
   * Create a simple default DivTable.
   */
  public SimpleDivTable() {
    this(false);
  }

  /**
   * Creates a DivTable that's loading by default (or not).
   *
   * @param loadingByDefault Whether this widget shows 'loading' content if empty.
   */
  public SimpleDivTable(final boolean loadingByDefault) {
    this(new TypedFlowPanel<SimpleDivTableRow>(), loadingByDefault);

  }

  /**
   * Create a DivTable with a custom row panel.
   *
   * @param rowPanel Custom row panel to use for rows.
   */
  public SimpleDivTable(final TypedRowPanel<SimpleDivTableRow> rowPanel) {
    this(rowPanel, false);
  }

  /**
   * Creates a DivTable that's loading by default (or not).
   *
   * @param rowPanel         Custom row panel to use for rows.
   * @param loadingByDefault Whether this widget shows 'loading' content if empty.
   */
  public SimpleDivTable(final TypedRowPanel<SimpleDivTableRow> rowPanel, final boolean loadingByDefault) {
    super(rowPanel, loadingByDefault);

    installRowCreator(v -> new SimpleDivTableRow());
  }
}
