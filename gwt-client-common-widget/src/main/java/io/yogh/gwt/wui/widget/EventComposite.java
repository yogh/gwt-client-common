package io.yogh.gwt.wui.widget;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;

public abstract class EventComposite extends DeferredComposite implements HasEventBus {
  protected EventBus eventBus;
  private HandlerRegistration registration;

  protected <T> HandlerRegistration bindEventHandlers(final T thiz, final EventBinder<T> binder) {
    return binder.bindEventHandlers(thiz, eventBus);
  }

  public <T> HandlerRegistration setEventBus(final EventBus eventBus, final T thiz, final EventBinder<T> binder,
      final HasEventBus... children) {
    setEventBus(eventBus, children);

    if (registration != null) {
      GWT.log("[INFO] REMOVING HANDLERS FROM PREVIOUS SETTING. THIS IS INTENDED BUT NON-ORDINARY.");
      registration.removeHandler();
    }

    registration = bindEventHandlers(thiz, binder);
    return registration;
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    this.eventBus = eventBus;
  }

  protected void setEventBus(final EventBus eventBus, final HasEventBus... composites) {
    this.eventBus = eventBus;

    for (final HasEventBus comp : composites) {
      comp.setEventBus(eventBus);
    }
  }
}