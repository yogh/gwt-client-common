package io.yogh.gwt.wui.widget.table;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

public interface DivTableRow extends IsWidget {
  HasWidgets asHasWidgets();

  default Widget asStyleWidget() {
    return asWidget();
  }
}
