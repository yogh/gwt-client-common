/*
 * Copyright Dutch Ministry of Economic Affairs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package io.yogh.gwt.wui.widget.table;

import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;

public class ColumnSortHeader<T extends SortableAttribute> extends Composite implements HasText, HasValueChangeHandlers<ColumnSortHeader<T>> {
  private static final int MARGIN_LEFT = 5;

  public interface SortActionHandler<T extends SortableAttribute> {
    void setSorting(T attribute, SortableDirection dir);
  }

  private final FlowPanel container = new FlowPanel();
  private final Label textField = new Label();
  private final HTML arrowField = new HTML();

  private SortActionHandler<T> handler;
  private T attribute;
  private SortableDirection dir;

  private final ClickHandler clickHandler = event -> {
    toggleDirection();
    handler.setSorting(attribute, dir);
  };

  public ColumnSortHeader() {
    this(null, null);
  }

  public ColumnSortHeader(final T attribute) {
    this(null, attribute);
  }

  public ColumnSortHeader(final SortActionHandler<T> handler) {
    this(handler, null);
  }

  public ColumnSortHeader(final SortActionHandler<T> handler, final T attribute) {
    this(handler, attribute, null);
  }

  public ColumnSortHeader(final SortActionHandler<T> handler, final T attribute, final String text) {
    this.handler = handler;
    this.attribute = attribute;

    initWidget(container);

    container.add(textField);
    container.add(arrowField);

    arrowField.getElement().getStyle().setMarginLeft(MARGIN_LEFT, Unit.PX);
    container.getElement().getStyle().setCursor(Cursor.POINTER);

    textField.getElement().getStyle().setProperty("textAlign", "inherit");

    container.getElement().getStyle().setProperty("userSelect", "none");
    container.getElement().getStyle().setProperty("display", "flex");
    container.getElement().getStyle().setProperty("alignItems", "center");

    addDomHandler(clickHandler, ClickEvent.getType());

    setText(text);
  }

  /**
   * @return the handler
   */
  public SortActionHandler<T> getHandler() {
    return handler;
  }

  /**
   * @return the attribute
   */
  public SortableAttribute getAttribute() {
    return attribute;
  }

  /**
   * @return the dir
   */
  public SortableDirection getDir() {
    return dir;
  }

  /**
   * @param handler the handler to set
   */
  public void setHandler(final SortActionHandler<T> handler) {
    this.handler = handler;
  }

  /**
   * @param attribute the attribute to set
   */
  public void setAttribute(final T attribute) {
    this.attribute = attribute;
  }

  /**
   * @param dir the dir to set
   */
  public void setDir(final SortableDirection dir) {
    this.dir = dir;

    updateDir();
  }

  @Override
  public String getText() {
    return textField.getText();
  }

  @Override
  public void setText(final String text) {
    textField.setText(text);
  }

  private void toggleDirection() {
    dir = dir == null ? SortableDirection.ASC
        : dir == SortableDirection.ASC
            ? SortableDirection.DESC
            : null;

    updateDir();

    ValueChangeEvent.fire(this, this);
  }

  private void updateDir() {
    if (dir == null) {
      arrowField.setHTML("");
      return;
    }

    switch (dir) {
    case DESC:
      arrowField.setHTML("&#9662;");
      break;
    case ASC:
    default:
      arrowField.setHTML("&#9652;");
      break;
    }
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<ColumnSortHeader<T>> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }
}