package io.yogh.gwt.wui.widget;

import com.google.gwt.dom.client.Document;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.ui.Widget;

public class MaterialIcon extends Widget {
  @UiConstructor
  public MaterialIcon() {
    setElement(Document.get().createElement("i"));
    setStyleName("material-icons");
  }

  public MaterialIcon(final String icon) {
    this();

    setIcon(icon);
  }

  public void setIcon(final String icon) {
    getElement().setInnerHTML(icon);
  }
}
