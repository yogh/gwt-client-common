package io.yogh.gwt.wui.widget;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;

public class RatioEditor extends DoubleEditor {
  @Override
  public void setValue(final Double value) {
    inner.setText(String.valueOf(value));
  }

  @Override
  public Double getValue() {
    return Math.max(Math.min(1, Double.parseDouble(inner.getValue())), 0);
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Double> handler) {
    return inner.addValueChangeHandler(e -> ValueChangeEvent.fire(RatioEditor.this, getValue()));
  }
}