/*
 * Copyright Dutch Ministry of Economic Affairs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package io.yogh.gwt.wui.widget.table;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

public class ColumnSortFlowPanel extends FlowPanel {
  private final ColumnSortHeaderBinder<? extends SortableAttribute> connector = new ColumnSortHeaderBinder<>();

  @SuppressWarnings({ "unchecked", "rawtypes" })
  @Override
  public void add(final Widget child) {
    super.add(child);

    if (child instanceof ColumnSortHeader) {
      connector.connect((ColumnSortHeader) child);
    }
  }
}
