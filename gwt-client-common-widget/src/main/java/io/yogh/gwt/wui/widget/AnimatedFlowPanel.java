package io.yogh.gwt.wui.widget;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

public class AnimatedFlowPanel extends FlowPanel {
  private static final int RECONCILIATION_DELAY = 155;

  private boolean globalAnimate = true;

  public AnimatedFlowPanel() {
    getElement().getStyle().setProperty("transition", "height .15s ease-out, opacity .15s ease-out");
  }

  public AnimatedFlowPanel(final boolean visible) {
    this();
    setVisible(visible, false);
  }

  @Override
  public void setVisible(final boolean visible) {
    setVisible(visible, true);
  }

  private void animate(final boolean visible) {
    if (visible) {
      super.setVisible(visible);
    }

    getElement().getStyle().setOverflow(Overflow.HIDDEN);

    final int startHeight = visible ? 0 : getOffsetHeight();
    final int endHeight = visible ? getOffsetHeight() : 0;

    final int startOpacity = visible ? 0 : 1;
    final int endOpacity = visible ? 1 : 0;

    getElement().getStyle().setHeight(startHeight, Unit.PX);
    getElement().getStyle().setOpacity(startOpacity);
    Scheduler.get().scheduleDeferred(() -> getElement().getStyle().setHeight(endHeight, Unit.PX));
    Scheduler.get().scheduleDeferred(() -> getElement().getStyle().setOpacity(endOpacity));
    Scheduler.get().scheduleFixedDelay(() -> postVisible(visible), RECONCILIATION_DELAY);
  }

  private boolean postVisible(final boolean visible) {
    super.setVisible(visible);
    getElement().getStyle().clearHeight();
    getElement().getStyle().setOverflow(Overflow.VISIBLE);

    return false;
  }

  public void setVisible(final boolean visible, final boolean animate) {
    if (!isAttached()) {
      super.setVisible(visible);
      return;
    }

    if (!animate || !globalAnimate) {
      super.setVisible(visible);
      return;
    }

    animate(visible);
  }

  public void setAnimate(final boolean animate) {
    this.globalAnimate = animate;
  }

  public void setWidget(final Widget widget) {
    clear();
    add(widget);
  }
}