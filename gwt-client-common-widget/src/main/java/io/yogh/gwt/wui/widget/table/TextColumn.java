
package io.yogh.gwt.wui.widget.table;

import java.util.function.Function;

import com.google.gwt.user.client.ui.Label;

public class TextColumn<L> extends WidgetFactory<L, Label> {
  private final boolean displayTitle;
  private Function<L, String> formatter;

  public TextColumn() {
    this(false);
  }

  public TextColumn(final Function<L, String> formatter) {
    this(formatter, false);
  }

  public TextColumn(final boolean displayTitle) {
    this.displayTitle = displayTitle;
  }

  public TextColumn(final Function<L, String> formatter, final boolean displayTitle) {
    this.formatter = formatter;
    this.displayTitle = displayTitle;
  }

  @Override
  public Label createWidget(final L object) {
    final Label label = new Label();

    final String value = getValue(object);

    if (value != null) {
      label.setText(value);
    }

    if (displayTitle) {
      final String titleText = getTitleText(object);
      label.setTitle(titleText == null ? value : titleText);
    }

    return label;
  }

  public String getTitleText(final L object) {
    return null;
  }

  public String getValue(final L object) {
    return formatter.apply(object);
  }
}
