package io.yogh.gwt.wui.widget.table;

import java.util.function.BiConsumer;

public interface SimpleDivTableRowDecorator<T> extends BiConsumer<T, SimpleDivTableRow> {}
