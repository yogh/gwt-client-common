package io.yogh.gwt.wui.widget;

public interface ContentSwitcher {
  void showWidget(int index);
}
