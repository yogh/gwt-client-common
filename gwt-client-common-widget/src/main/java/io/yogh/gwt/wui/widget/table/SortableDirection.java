package io.yogh.gwt.wui.widget.table;

public enum SortableDirection {
  ASC, DESC;
}
