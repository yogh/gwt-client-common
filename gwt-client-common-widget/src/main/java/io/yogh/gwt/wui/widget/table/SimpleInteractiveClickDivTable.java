package io.yogh.gwt.wui.widget.table;

public class SimpleInteractiveClickDivTable<T> extends InteractiveClickDivTable<T, SimpleInteractiveDivTableRow> {
  /**
   * Create a simple default DivTable.
   */
  public SimpleInteractiveClickDivTable() {
    this(false);
  }

  /**
   * Creates a DivTable that's loading by default (or not).
   *
   * @param loadingByDefault Whether this widget shows 'loading' content if empty.
   */
  public SimpleInteractiveClickDivTable(final boolean loadingByDefault) {
    this(new TypedFlowPanel<SimpleInteractiveDivTableRow>(), loadingByDefault);

  }

  /**
   * Create a DivTable with a custom row panel.
   *
   * @param rowPanel Custom row panel to use for rows.
   */
  public SimpleInteractiveClickDivTable(final TypedRowPanel<SimpleInteractiveDivTableRow> rowPanel) {
    this(rowPanel, false);
  }

  /**
   * Creates a DivTable that's loading by default (or not).
   *
   * @param rowPanel         Custom row panel to use for rows.
   * @param loadingByDefault Whether this widget shows 'loading' content if empty.
   */
  public SimpleInteractiveClickDivTable(final TypedRowPanel<SimpleInteractiveDivTableRow> rowPanel, final boolean loadingByDefault) {
    super(rowPanel, loadingByDefault);

    installRowCreator(v -> new SimpleInteractiveDivTableRow());
  }
}
