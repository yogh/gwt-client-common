package io.yogh.gwt.wui.widget.table;

import java.util.function.Consumer;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

public class SimpleWrappingInteractiveDivTableRow extends SimpleWrappingDivTableRow implements InteractiveDivTableRow {
  public SimpleWrappingInteractiveDivTableRow() {
    super(new FlowPanel());
  }

  public SimpleWrappingInteractiveDivTableRow(final FlowPanel panel) {
    super(panel);
  }

  public SimpleWrappingInteractiveDivTableRow(final Consumer<Widget> rowDecorator) {
    super(rowDecorator);
  }

  private boolean value;

  @Override
  public Boolean getValue() {
    return value;
  }

  @Override
  public void setValue(final Boolean value) {
    setValue(value, false);
  }

  @Override
  public void setValue(final Boolean value, final boolean fireEvents) {
    this.value = value;

    if (fireEvents) {
      ValueChangeEvent.fire(this, value);
    }
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Boolean> handler) {
    return container.addHandler(handler, ValueChangeEvent.getType());
  }

  @Override
  public void fireEvent(final GwtEvent<?> event) {
    container.fireEvent(event);
  }
}
