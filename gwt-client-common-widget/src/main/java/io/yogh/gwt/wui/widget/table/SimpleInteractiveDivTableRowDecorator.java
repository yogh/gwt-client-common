package io.yogh.gwt.wui.widget.table;

import java.util.function.BiConsumer;

public interface SimpleInteractiveDivTableRowDecorator<T> extends BiConsumer<T, SimpleInteractiveDivTableRow> {}
