package io.yogh.gwt.wui.widget;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

import com.google.gwt.editor.client.LeafValueEditor;

public class FakeLeafValueEditor<T> implements LeafValueEditor<T> {
  private T value;
  private final Optional<Supplier<T>> getValue;
  private final Optional<Consumer<T>> setValue;

  public FakeLeafValueEditor() {
    this.getValue = Optional.empty();
    this.setValue = Optional.empty();
  }

  public FakeLeafValueEditor(final Consumer<T> setValue) {
    this.setValue = Optional.of(setValue);
    this.getValue = Optional.empty();
  }

  public FakeLeafValueEditor(final Consumer<T> setValue, final Supplier<T> getValue) {
    this.setValue = Optional.of(setValue);
    this.getValue = Optional.of(getValue);
  }

  @Override
  public T getValue() {
    return getValue.map(Supplier::get).orElse(value);
  }

  @Override
  public void setValue(final T value) {
    if (setValue.isPresent()) {
      setValue.get().accept(value);
    }

    this.value = value;
  }
}
