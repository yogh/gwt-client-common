package io.yogh.gwt.wui.widget;

import io.yogh.gwt.wui.activity.View;

public class EventCompositeView<P> extends EventComposite implements View<P> {
  protected P presenter;

  @Override
  public void setPresenter(final P presenter) {
    this.presenter = presenter;
  }
}
