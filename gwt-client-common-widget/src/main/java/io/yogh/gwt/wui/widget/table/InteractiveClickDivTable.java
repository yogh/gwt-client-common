package io.yogh.gwt.wui.widget.table;

import com.google.gwt.event.dom.client.ClickEvent;

public abstract class InteractiveClickDivTable<T, R extends InteractiveDivTableRow> extends InteractiveDivTable<T, R> {
  /**
   * Create a simple default DivTable.
   */
  public InteractiveClickDivTable() {
    this(false);
  }

  /**
   * Creates a DivTable that's loading by default (or not).
   *
   * @param loadingByDefault Whether this widget shows 'loading' content if empty.
   */
  public InteractiveClickDivTable(final boolean loadingByDefault) {
    this(new TypedFlowPanel<R>(), loadingByDefault);

  }

  /**
   * Create a DivTable with a custom row panel.
   *
   * @param rowPanel Custom row panel to use for rows.
   */
  public InteractiveClickDivTable(final TypedRowPanel<R> rowPanel) {
    this(rowPanel, false);
  }

  /**
   * Creates a DivTable that's loading by default (or not).
   *
   * @param rowPanel         Custom row panel to use for rows.
   * @param loadingByDefault Whether this widget shows 'loading' content if empty.
   */
  public InteractiveClickDivTable(final TypedRowPanel<R> rowPanel, final boolean loadingByDefault) {
    super(rowPanel, loadingByDefault);
  }

  @Override
  public void accept(final T item, final R row) {
    super.accept(item, row);

    row.asWidget().addDomHandler(e -> toggleRowSelection(row, item), ClickEvent.getType());
  }
}
