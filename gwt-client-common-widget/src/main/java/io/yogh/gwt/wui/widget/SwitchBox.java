package io.yogh.gwt.wui.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.InputElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class SwitchBox extends Composite implements HasValueChangeHandlers<Boolean>, LeafValueEditor<Boolean> {
  private static final SwitchBoxUiBinder UI_BINDER = GWT.create(SwitchBoxUiBinder.class);

  interface SwitchBoxUiBinder extends UiBinder<Widget, SwitchBox> {}

  @UiField InputElement input;
  @UiField SpanElement slider;

  private String colorLeft;
  private String colorRight;

  public SwitchBox() {
    initWidget(UI_BINDER.createAndBindUi(this));

    Event.sinkEvents(input, Event.ONCHANGE);
    Event.setEventListener(input, e -> {
      if ((e.getTypeInt() & Event.ONCHANGE) == Event.ONCHANGE) {
        final boolean checked = getValue();
        ValueChangeEvent.fire(SwitchBox.this, checked);
        slider.getStyle().setBackgroundColor(checked ? colorRight : colorLeft);
      }
    });
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Boolean> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  public void setLeft(final String colorLeft) {
    this.colorLeft = colorLeft;
  }

  public void setRight(final String colorRight) {
    this.colorRight = colorRight;
  }

  @Override
  public void setValue(final Boolean value) {
    input.setChecked(value);
    ValueChangeEvent.fire(SwitchBox.this, value);
    slider.getStyle().setBackgroundColor(value ? colorRight : colorLeft);
  }

  @Override
  public Boolean getValue() {
    return input.isChecked();
  }
}