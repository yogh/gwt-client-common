package io.yogh.gwt.wui.widget.table;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

public class ExpansiveRowPanel extends Composite implements TypedRowPanel<SimpleInteractiveDivTableRow> {
  private static final ExpansiveRowPanelUiBinder UI_BINDER = GWT.create(ExpansiveRowPanelUiBinder.class);

  interface ExpansiveRowPanelUiBinder extends UiBinder<Widget, ExpansiveRowPanel> {}

  @UiField FlowPanel panel;

  public ExpansiveRowPanel() {
    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  public int size() {
    return panel.getWidgetCount();
  }

  @Override
  public SimpleInteractiveDivTableRow getWidget(final int i) {
    return (SimpleInteractiveDivTableRow) panel.getWidget(i);
  }

  @Override
  public void insert(final SimpleInteractiveDivTableRow row, final int index) {
    panel.insert(row, index);
  }

  @Override
  public void clear() {
    panel.clear();
  }
}
