package io.yogh.gwt.wui.widget.table;

public final class ColumnSortHeaderBinderUtil {
  private ColumnSortHeaderBinderUtil() {}

  /**
   * Connect the given ColumnSortHeaders - allowing for only a single one of them to be active at any time.
   *
   * @param headers Headers to connect - be sure they are the same type.
   *
   * @param <T> The set of attributes that are sortable.
   *
   * @return A ColumnSortHeaderBinder you may use to disconnect or add connections.
   */
  @SafeVarargs
  public static <T extends SortableAttribute> ColumnSortHeaderBinder<T> connect(final ColumnSortHeader<T>... headers) {
    final ColumnSortHeaderBinder<T> binder = new ColumnSortHeaderBinder<T>();

    for (final ColumnSortHeader<T> header : headers) {
      binder.connect(header);
    }

    return binder;
  }
}