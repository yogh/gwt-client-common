package io.yogh.gwt.wui.widget;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public abstract class DeferredComposite extends Composite {
  @Override
  protected void initWidget(final Widget widget) {
    super.initWidget(widget);

    initWidget();
  }

  protected void initWidget() {}
}
