package io.yogh.gwt.wui.widget;

import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;

public class DoubleEditor extends Composite implements HasValueChangeHandlers<Double>, LeafValueEditor<Double> {
  protected final TextBox inner = new TextBox();

  public DoubleEditor() {
    initWidget(inner);
  }

  @Override
  public void setValue(final Double value) {
    inner.setText(String.valueOf(value));
  }

  @Override
  public Double getValue() {
    return Double.parseDouble(inner.getText());
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Double> handler) {
    return inner.addValueChangeHandler(e -> ValueChangeEvent.fire(DoubleEditor.this, Double.parseDouble(e.getValue())));
  }
}