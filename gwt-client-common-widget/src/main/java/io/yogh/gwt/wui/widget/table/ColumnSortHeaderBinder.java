package io.yogh.gwt.wui.widget.table;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;

public class ColumnSortHeaderBinder<T extends SortableAttribute> implements ValueChangeHandler<ColumnSortHeader<T>> {
  private ColumnSortHeader<T> activeSorter;

  public ColumnSortHeaderBinder() {}

  public void connect(final ColumnSortHeader<T> header) {
    header.addValueChangeHandler(this);
  }

  @Override
  public void onValueChange(final ValueChangeEvent<ColumnSortHeader<T>> event) {
    if ((activeSorter != null) && !activeSorter.equals(event.getValue())) {
      activeSorter.setDir(null);
    }

    activeSorter = event.getValue();
  }
}