package io.yogh.gwt.wui.widget.table;

import java.util.function.Function;

public class CurrencyColumn<L> extends DoubleColumn<L> {
  private static final String NOT_AVAILABLE_VALUE = "-";

  public CurrencyColumn(final Function<L, Double> formatter) {
    super(formatter, 2);
  }

  @Override
  protected String formatValue(final Double value) {
    if (value == null) {
      return NOT_AVAILABLE_VALUE;
    }

    return String.valueOf("€ " + roundValue(value));
  }
}
