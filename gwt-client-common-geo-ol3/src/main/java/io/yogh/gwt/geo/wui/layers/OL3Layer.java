package io.yogh.gwt.geo.wui.layers;

import java.util.Optional;

import io.yogh.gwt.geo.domain.IsLayer;
import io.yogh.gwt.geo.domain.LayerInfo;
import ol.layer.Layer;

public class OL3Layer implements IsLayer<Layer> {
  private final Layer layer;
  private final LayerInfo info;
  private final Runnable retirement;

  public OL3Layer(final Layer layer) {
    this(layer, null, null);
  }

  public OL3Layer(final Layer layer, final LayerInfo info) {
    this(layer, info, null);
  }

  public OL3Layer(final Layer layer, final LayerInfo info, final Runnable retirement) {
    if (layer == null) {
      throw new RuntimeException("Layer is null.");
    }

    this.layer = layer;
    this.info = info;
    this.retirement = retirement;
  }

  @Override
  public Layer asLayer() {
    return layer;
  }

  @Override
  public Optional<LayerInfo> getInfo() {
    return Optional.of(info);
  }

  public void retire() {
    if (retirement != null) {
      retirement.run();
    }
  }

  @Override
  public String toString() {
    return "OL3Layer " + (info == null ? "" : info.getTitle()) + "]";
  }
}
