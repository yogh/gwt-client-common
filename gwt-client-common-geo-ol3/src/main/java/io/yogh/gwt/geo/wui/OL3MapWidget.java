package io.yogh.gwt.geo.wui;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import io.yogh.gwt.geo.domain.IsMapCohort;
import io.yogh.gwt.geo.event.MapEventBus;

import jsinterop.base.Js;

public class OL3MapWidget extends Composite implements Map {
  private String uniqueId;

  private final MapLayoutPanel map;

  private boolean attached;

  private MapEventBus eventBus;

  private final List<IsMapCohort> cohorts = new ArrayList<>();

  @Inject
  public OL3MapWidget(final MapLayoutPanel map) {
    this.map = map;
    final SimplePanel container = new SimplePanel();
    container.getElement().getStyle().setPosition(Position.RELATIVE);

    initWidget(container);

    container.getElement().getStyle().setWidth(100, Unit.PCT);
    container.getElement().getStyle().setHeight(100, Unit.PCT);

    container.getElement().getStyle().setCursor(Cursor.POINTER);

    uniqueId = "openlayers-3-map-auto-generated-" + DOM.createUniqueId();
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    getElement().setId(uniqueId);
    this.eventBus = new MapEventBus(eventBus, uniqueId);
    map.setEventBus(this.eventBus);

    notifyCohorts();
  }

  @Override
  protected void onLoad() {
    if (attached) {
      map.updateSize();
    }
  }

  @Override
  public void attach() {
    if (attached) {
      return;
    }

    map.setTarget(uniqueId);

    attached = true;
  }

  private void notifyCohorts() {
    for (final IsMapCohort cohort : cohorts) {
      cohort.notifyMap(this, eventBus);
    }
  }

  @Override
  public void registerEventCohort(final IsMapCohort cohort) {
    if (eventBus != null) {
      cohort.notifyMap(this, eventBus);
    }

    cohorts.add(cohort);
  }

  @Override
  public void setUniqueId(final String uniqueId) {
    this.uniqueId = uniqueId;
  }

  @Override
  public String getUniqueId() {
    return uniqueId;
  }

  @Override
  public <T> T getMap() {
    return Js.cast(map.getMap());
  }
}
