package io.yogh.gwt.geo;

import com.google.gwt.inject.client.AbstractGinModule;
import com.google.gwt.inject.client.assistedinject.GinFactoryModuleBuilder;
import com.google.inject.TypeLiteral;

import io.yogh.gwt.geo.util.MapLayerFactory;
import io.yogh.gwt.geo.wui.util.OL3MapLayerFactory;

/**
 * Gin bindings for the Monitor Up application.
 */
public class BareOpenLayers3ClientModule extends AbstractGinModule {
  @Override
  protected void configure() {
    install(new GinFactoryModuleBuilder().build(OL3MapWidgetFactory.class));
    bind(MapBuilder.class).to(OL3MapWidgetBuilder.class);

    bind(new TypeLiteral<MapLayerFactory<?>>() {}).to(OL3MapLayerFactory.class);
  }
}
