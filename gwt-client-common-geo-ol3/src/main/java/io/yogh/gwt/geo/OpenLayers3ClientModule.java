package io.yogh.gwt.geo;

import io.yogh.gwt.geo.wui.Map;
import io.yogh.gwt.geo.wui.OL3MapWidget;

/**
 * Gin bindings for the Monitor Up application.
 */
public class OpenLayers3ClientModule extends BareOpenLayers3ClientModule {
  @Override
  protected void configure() {
    super.configure();

    bind(Map.class).to(OL3MapWidget.class);
  }
}
