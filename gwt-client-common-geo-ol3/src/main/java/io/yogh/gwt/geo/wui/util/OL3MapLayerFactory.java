package io.yogh.gwt.geo.wui.util;

import com.google.web.bindery.event.shared.EventBus;

import io.yogh.gwt.geo.domain.IsLayer;
import io.yogh.gwt.geo.domain.LayerInfo;
import io.yogh.gwt.geo.domain.layer.IsLayerConfiguration;
import io.yogh.gwt.geo.domain.layer.WMSConfiguration;
import io.yogh.gwt.geo.util.MapLayerFactory;
import io.yogh.gwt.geo.wui.layers.OL3Layer;
import ol.OLFactory;
import ol.layer.Image;
import ol.layer.Layer;
import ol.layer.LayerOptions;
import ol.source.ImageWms;
import ol.source.ImageWmsOptions;
import ol.source.ImageWmsParams;

public final class OL3MapLayerFactory implements MapLayerFactory<Layer> {
  public static IsLayer<Layer> wrap(final Layer layer) {
    return new OL3Layer(layer);
  }

  public static IsLayer<Layer> wrap(final Layer layer, final LayerInfo info, final Runnable retirement) {
    return new OL3Layer(layer, info, retirement);
  }

  @Override
  public IsLayer<Layer> prepareLayer(final IsLayerConfiguration c, final EventBus eventBus) {
    if (c instanceof WMSConfiguration) {
      return prepareWMSLayer((WMSConfiguration) c, eventBus);
    } else {
      throw new RuntimeException("Unsupported layer configuration: " + c.getClass().getSimpleName());
    }
  }

  public IsLayer<Layer> prepareWMSLayer(final WMSConfiguration c, final EventBus eventBus) {
    final ImageWmsParams imageWMSParams = OLFactory.createOptions();

    imageWMSParams.setLayers(c.baseLayer());
    imageWMSParams.setFormat(c.formats());

    final ImageWmsOptions imageWMSOptions = OLFactory.createOptions();
    imageWMSOptions.setUrl(c.url());
    imageWMSOptions.setParams(imageWMSParams);

    final ImageWms imageWMSSource = new ImageWms(imageWMSOptions);

    final LayerOptions layerOptions = OLFactory.createOptions();
    layerOptions.setSource(imageWMSSource);

    final Image wmsLayer = new Image(layerOptions);
    wmsLayer.setOpacity(c.layer().opacity());
    wmsLayer.setVisible(c.layer().visible());

    final LayerInfo info = new LayerInfo();
    info.setName(c.layer().name());
    info.setTitle(c.layer().title());

    return wrap(wmsLayer, info, () -> {});
  }
};
