package io.yogh.gwt.geo;

import io.yogh.gwt.geo.wui.OL3MapWidget;

public interface OL3MapWidgetFactory {
  OL3MapWidget getMap();
}
