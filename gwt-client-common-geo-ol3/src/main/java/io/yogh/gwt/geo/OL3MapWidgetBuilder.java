package io.yogh.gwt.geo;

import com.google.inject.Inject;

import io.yogh.gwt.geo.wui.OL3MapWidget;

public class OL3MapWidgetBuilder implements MapBuilder {
  private final OL3MapWidgetFactory mapFactory;

  @Inject
  public OL3MapWidgetBuilder(final OL3MapWidgetFactory mapFactory) {
    this.mapFactory = mapFactory;
  }

  @Override
  public OL3MapWidget getMap() {
    return mapFactory.getMap();
  }
}
