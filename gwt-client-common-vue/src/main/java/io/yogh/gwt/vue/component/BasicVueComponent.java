/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package io.yogh.gwt.vue.component;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.gwt.resources.client.DataResource;

import jsinterop.annotations.JsMethod;

@Component(hasTemplate = false)
public abstract class BasicVueComponent implements IsVueComponent {
  public void destroy() {
    vue().$destroy();
    try {
      vue().$el().parentNode.removeChild(vue().$el());
    } catch (final Exception e) {
      // Eat
    }
  }

  @JsMethod
  public String mask(final DataResource res) {
    final String url = "url(" + res.getSafeUri().asString() + ")";
    return "mask:" + url + ";-webkit-mask:" + url + ";-webkit-mask-image:" + url + ";";
  }

  /**
   * Dummy method that does nothing. VueGWT doesn't support empty expressions, using this dummy makes that easier.
   */
  @JsMethod
  public void dummy() {}
}
