package io.yogh.gwt.vue.activity;

import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.vue.VueComponentFactory;

import io.yogh.gwt.vue.AcceptsOneComponent;
import io.yogh.gwt.wui.activity.Activity;
import io.yogh.gwt.wui.event.BasicEventComponent;
import io.yogh.gwt.wui.widget.HasEventBus;

public abstract class AbstractVueActivity<P, V extends IsVueComponent, F extends VueComponentFactory<V>>
    extends BasicEventComponent implements Activity<P, AcceptsOneComponent>, HasEventBus {
  private final F factory;

  public AbstractVueActivity(final F factory) {
    this.factory = factory;
  }

  @Override
  public void onStop() {}

  @Override
  public String mayStop() {
    return null;
  }

  public void setView(final V view) {}

  @Override
  public void onStart(final AcceptsOneComponent panel) {
    if (panel instanceof HasEventBus) {
      ((HasEventBus) panel).setEventBus(eventBus);
    }
    panel.setComponent(factory, getPresenter());
    onStart();
  }

  /**
   * A convenience method that can be overridden when the panel is irrelevant.
   */
  @Override
  public void onStart() {
    // No-op
  }

  @Override
  public abstract P getPresenter();
}
