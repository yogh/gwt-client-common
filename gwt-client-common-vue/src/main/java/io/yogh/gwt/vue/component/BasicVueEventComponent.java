/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package io.yogh.gwt.vue.component;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;

import io.yogh.gwt.wui.dev.GWTProd;
import io.yogh.gwt.wui.widget.HasEventBus;

@Component(hasTemplate = false)
public abstract class BasicVueEventComponent extends BasicVueComponent implements IsVueComponent, HasCreated, HasEventBus {
  @Prop public EventBus eventBus;

  private HandlerRegistration bindEventHandlers;

  @Override
  public void created() {
    setEventBus(eventBus);
  }

  public <T> HandlerRegistration setEventBus(final EventBus eventBus, final T thiz, final EventBinder<T> binder,
      final HasEventBus... children) {
    setEventBus(eventBus, children);

    if (bindEventHandlers != null) {
      throw new RuntimeException("Existing handlers are not null which means a bug has been implemented.");
    }

    bindEventHandlers = binder.bindEventHandlers(thiz, eventBus);

    return bindEventHandlers;
  }

  @Override
  public void destroy() {
    GWTProd.log("Destroying");
    bindEventHandlers.removeHandler();
    bindEventHandlers = null;
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    // Optional override
  }

  public void setEventBus(final EventBus eventBus, final HasEventBus... children) {
    for (final HasEventBus child : children) {
      child.setEventBus(eventBus);
    }
  }
}
