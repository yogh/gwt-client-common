package io.yogh.gwt.vue;


import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.vue.VueComponentFactory;
import com.google.inject.Provider;

public class StaticVueProvider<T extends IsVueComponent> implements Provider<T> {
  private final VueComponentFactory<? extends T> factory;

  public StaticVueProvider(final VueComponentFactory<? extends T> factory) {
    this.factory = factory;
  }

  @Override
  public T get() {
    return factory.create();
  }
}
