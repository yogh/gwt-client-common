package io.yogh.gwt.vue.activity;

import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import io.yogh.gwt.vue.AcceptsOneComponent;
import io.yogh.gwt.wui.activity.AbstractActivityManager;
import io.yogh.gwt.wui.activity.ActivityMapper;
import io.yogh.gwt.wui.place.PlaceController;


public class VueActivityManager extends AbstractActivityManager<AcceptsOneComponent> {
  @Inject
  public VueActivityManager(final EventBus globalEventBus, final PlaceController placeController, final ActivityMapper<AcceptsOneComponent> mapper) {
    super(globalEventBus, placeController, mapper);
  }
}
