package io.yogh.gwt.geo.event;

import io.yogh.gwt.geo.event.LayerChangeEvent.CHANGE;

public interface HasLayerChange {
  CHANGE getChange();
}
