package io.yogh.gwt.geo.command;

import io.yogh.gwt.geo.event.MapSetExtentEvent;
import io.yogh.gwt.wui.command.SimpleGenericCommand;

public class MapSetExtentCommand extends SimpleGenericCommand<String, MapSetExtentEvent> {
  private boolean userInitiated;
  private boolean fromMap;

  public MapSetExtentCommand(final String extent) {
    super(extent);
  }

  public MapSetExtentCommand(final String extent, final boolean userInitiated) {
    super(extent);
    this.userInitiated = userInitiated;
  }

  public MapSetExtentCommand(final String extent, final boolean userInitiated, final boolean fromMap) {
    super(extent);
    this.userInitiated = userInitiated;
    this.fromMap = fromMap;
  }

  @Override
  protected MapSetExtentEvent createEvent(final String value) {
    return new MapSetExtentEvent(value);
  }

  public boolean isUserInitiated() {
    return userInitiated;
  }

  public void setUserInitiated(final boolean userInitiated) {
    this.userInitiated = userInitiated;
  }

  public boolean isFromMap() {
    return fromMap;
  }

  public void setFromMap(final boolean fromMap) {
    this.fromMap = fromMap;
  }
}
