package io.yogh.gwt.geo.domain;

import io.yogh.gwt.geo.event.MapEventBus;
import io.yogh.gwt.geo.wui.Map;

public interface IsMapCohort {
  void notifyMap(Map map, MapEventBus mapEventBus);
}
