package io.yogh.gwt.geo.event;

import io.yogh.gwt.geo.domain.IsLayer;

public class LayerRemovedEvent extends LayerChangeEvent {
  public LayerRemovedEvent(final IsLayer<?> layer) {
    super(layer, CHANGE.REMOVED);
  }

}
