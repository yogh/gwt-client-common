package io.yogh.gwt.geo.domain;

public interface HasMapCohort {
  IsMapCohort asMapCohort();
}
