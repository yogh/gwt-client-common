package io.yogh.gwt.geo.epsg;

import com.google.inject.Singleton;

import io.yogh.gwt.geo.BBox;

/**
 * Constants for RD new coordinates.
 */
@Singleton
public final class EPSGRDNew extends EPSG {
  public static final int SRID = 28992;

  private static final double[] RESOLUTIONS =
    { 13762.56, 6881.28, 3440.64, 1720.32, 860.16, 430.08, 215.04, 107.52, 53.76, 26.88, 13.44, 6.72, 3.36 };
  private static final float MAX_RESOLUTION = 0;
  private static final int ZOOM_LEVEL = 14;
  private static final String UNIT = "km";
  private static final BBox BOUNDS = new BBox(-285401.920, 22598.080, 595401.920, 903401.920);

  public EPSGRDNew() {
    super(SRID, BOUNDS, RESOLUTIONS, MAX_RESOLUTION, UNIT, ZOOM_LEVEL);
  }
}
