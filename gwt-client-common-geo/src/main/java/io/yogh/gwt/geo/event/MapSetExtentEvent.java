package io.yogh.gwt.geo.event;

import io.yogh.gwt.wui.event.SimpleGenericEvent;

public class MapSetExtentEvent extends SimpleGenericEvent<String> {
  public MapSetExtentEvent(final String extent) {
    super(extent);
  }
}
