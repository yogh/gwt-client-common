package io.yogh.gwt.geo.util;

import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import io.yogh.gwt.geo.domain.IsLayer;
import io.yogh.gwt.geo.domain.layer.IsLayerConfiguration;

public class MapLayerUtil {
  public static MapLayerUtil I;

  private final MapLayerFactory<?> util;

  @Inject
  public MapLayerUtil(final MapLayerFactory<?> util) {
    this.util = util;

    I = this;
  }

  public static IsLayer<?> prepareLayer(final IsLayerConfiguration conf, final EventBus eventBus) {
    return I.util.prepareLayer(conf, eventBus);
  }
}
