package io.yogh.gwt.geo.domain.layer;

public interface IsLayerConfiguration {
  LayerConfiguration layer();
}
