package io.yogh.gwt.geo.event;

import io.yogh.gwt.geo.domain.IsLayer;

public class LayerAddedEvent extends LayerChangeEvent {
  public LayerAddedEvent(final IsLayer<?> layer) {
    super(layer, CHANGE.ADDED);
  }
}
