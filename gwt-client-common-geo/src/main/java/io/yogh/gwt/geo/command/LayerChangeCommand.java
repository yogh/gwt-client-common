package io.yogh.gwt.geo.command;

import io.yogh.gwt.geo.domain.IsLayer;
import io.yogh.gwt.geo.event.HasLayerChange;
import io.yogh.gwt.geo.event.LayerChangeEvent;
import io.yogh.gwt.geo.event.LayerChangeEvent.CHANGE;
import io.yogh.gwt.wui.command.SimpleGenericCommand;

public abstract class LayerChangeCommand<E extends LayerChangeEvent> extends SimpleGenericCommand<IsLayer<?>, E> implements HasLayerChange {
  private final CHANGE change;

  /**
   * Creates a {@link LayerChangeCommand}.
   *
   * @param change layer change event
   */
  public LayerChangeCommand(final IsLayer<?> layer, final CHANGE change) {
    super(layer);
    this.change = change;
  }

  @SuppressWarnings("unchecked")
  public <T> T getLayer() {
    return (T) getIsLayer().asLayer();
  }

  @SuppressWarnings("unchecked")
  public <T> IsLayer<T> getIsLayer() {
    return (IsLayer<T>) getValue();
  }

  /**
   * Returns the layer change event.
   *
   * @return change event
   */
  @Override
  public CHANGE getChange() {
    return change;
  }
}