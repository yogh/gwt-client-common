package io.yogh.gwt.geo.event;

import io.yogh.gwt.geo.domain.IsLayer;

public class LayerHiddenEvent extends LayerChangeEvent {
  public LayerHiddenEvent(final IsLayer<?> layer) {
    super(layer, CHANGE.HIDDEN);
  }

}
