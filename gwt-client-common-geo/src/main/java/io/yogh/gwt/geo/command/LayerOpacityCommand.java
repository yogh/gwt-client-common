package io.yogh.gwt.geo.command;

import io.yogh.gwt.geo.domain.IsLayer;
import io.yogh.gwt.geo.event.LayerChangeEvent.CHANGE;
import io.yogh.gwt.geo.event.LayerOpacityEvent;

public class LayerOpacityCommand extends LayerChangeCommand<LayerOpacityEvent> {
  private final double opacity;

  public LayerOpacityCommand(final IsLayer<?> layer, final double opacity) {
    super(layer, CHANGE.OPACITY);
    this.opacity = opacity;

  }

  @Override
  protected LayerOpacityEvent createEvent(final IsLayer<?> value) {
    return new LayerOpacityEvent(value, opacity);
  }

  public double getOpacity() {
    return opacity;
  }
}
