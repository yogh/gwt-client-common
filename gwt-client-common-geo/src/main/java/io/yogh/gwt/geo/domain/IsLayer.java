package io.yogh.gwt.geo.domain;

import java.util.Optional;

public interface IsLayer<L> {
  L asLayer();

  default Optional<LayerInfo> getInfo() {
    return Optional.empty();
  }
}
