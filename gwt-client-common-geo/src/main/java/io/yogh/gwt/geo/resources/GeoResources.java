package io.yogh.gwt.geo.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.DataResource.MimeType;

public interface GeoResources extends ClientBundle {

  @Source("images/toggle-layer-on.svg")
  @MimeType("image/svg+xml")
  DataResource toggleLayerOn();

  @Source("images/toggle-layer-off.svg")
  @MimeType("image/svg+xml")
  DataResource toggleLayerOff();

}
