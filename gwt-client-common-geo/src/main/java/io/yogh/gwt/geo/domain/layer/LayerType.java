package io.yogh.gwt.geo.domain.layer;

public enum LayerType {
  WMS, WFS
}
