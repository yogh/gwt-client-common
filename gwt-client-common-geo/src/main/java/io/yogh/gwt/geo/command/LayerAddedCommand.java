package io.yogh.gwt.geo.command;

import io.yogh.gwt.geo.domain.IsLayer;
import io.yogh.gwt.geo.event.LayerAddedEvent;
import io.yogh.gwt.geo.event.LayerChangeEvent.CHANGE;

public class LayerAddedCommand extends LayerChangeCommand<LayerAddedEvent> {
  public LayerAddedCommand(final IsLayer<?> layer) {
    super(layer, CHANGE.ADDED);
  }

  @Override
  protected LayerAddedEvent createEvent(final IsLayer<?> value) {
    return new LayerAddedEvent(value);
  }
}
