package io.yogh.gwt.geo.wui;

import io.yogh.gwt.geo.domain.HasMapCohort;
import io.yogh.gwt.geo.domain.IsMapCohort;
import io.yogh.gwt.wui.widget.HasEventBus;

public interface Map extends HasEventBus {
  void attach();

  void registerEventCohort(IsMapCohort mapCohort);

  default void registerEventCohort(final HasMapCohort mapCohort) {
    registerEventCohort(mapCohort.asMapCohort());
  }

  String getUniqueId();

  void setUniqueId(String uniqueId);

  <T> T getMap();
}
