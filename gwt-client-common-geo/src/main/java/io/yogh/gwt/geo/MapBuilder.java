package io.yogh.gwt.geo;

import io.yogh.gwt.geo.wui.Map;

public interface MapBuilder {
  Map getMap();
}
