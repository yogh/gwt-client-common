package io.yogh.gwt.geo.event;

import io.yogh.gwt.geo.domain.IsLayer;
import io.yogh.gwt.wui.event.SimpleGenericEvent;

public abstract class LayerChangeEvent extends SimpleGenericEvent<IsLayer<?>> implements HasLayerChange {
  /**
   * A type of change.
   */
  public static enum CHANGE {
    /**
     * Layer has been added to the layer panel.
     */
    ADDED,
    /**
     * Layer has been disabled.
     */
    DISABLED,
    /**
     * Layer has been enabled.
     */
    ENABLED,
    /**
     * Conditions changed such that the layer has no data to display and has been hidden.
     */
    HIDDEN,
    /**
     * Layer name changed.
     */
    NAME,
    /**
     * Layer opacity changed.
     */
    OPACITY,
    /**
     * Layer order changed in the layer panel.
     */
    ORDER,
    /**
     * Layer has been removed from the layer panel.
     */
    REMOVED,
    /**
     * Conditions changed such that data for the layer is now available and the layer has become visible.
     */
    VISIBLE,

    /**
     * The legend for this layer changed.
     */
    LEGEND_CHANGED;
  }

  private final CHANGE change;

  /**
   * Creates a {@link LayerChangeEvent}.
   *
   * @param layer the Layer to show/hide
   * @param change layer change event
   */
  public LayerChangeEvent(final IsLayer<?> layer, final CHANGE change) {
    super(layer);
    this.change = change;
  }

  @SuppressWarnings("unchecked")
  public <T> T getLayer() {
    return ((IsLayer<T>) getValue()).asLayer();
  }

  /**
   * Returns the layer change event.
   *
   * @return change event
   */
  @Override
  public CHANGE getChange() {
    return change;
  }

  @Override
  public String toString() {
    return "LayerChangeEvent [change=" + change + "]";
  }
}