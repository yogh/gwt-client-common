package io.yogh.gwt.geo.command;

import io.yogh.gwt.geo.domain.IsLayer;
import io.yogh.gwt.geo.event.LayerChangeEvent.CHANGE;
import io.yogh.gwt.geo.event.LayerHiddenEvent;

public class LayerHiddenCommand extends LayerChangeCommand<LayerHiddenEvent> {
  public LayerHiddenCommand(final IsLayer<?> layer) {
    super(layer, CHANGE.HIDDEN);
  }

  @Override
  protected LayerHiddenEvent createEvent(final IsLayer<?> value) {
    return new LayerHiddenEvent(value);
  }
}
