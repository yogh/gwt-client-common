package io.yogh.gwt.geo.domain.layer;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class WMSConfiguration implements IsLayerConfiguration {
  public static Builder builder() {
    return new AutoValue_WMSConfiguration.Builder()
        .viewParams("")
        .cqlFilter("")
        .baseStyle("");
  }

  @Override
  public abstract LayerConfiguration layer();

  public abstract String url();

  public abstract String baseLayer();

  public abstract String baseStyle();

  public abstract String formats();

  public abstract String cqlFilter();

  public abstract String viewParams();

  @AutoValue.Builder
  public static abstract class Builder {
    public abstract Builder layer(LayerConfiguration value);

    public abstract Builder url(String value);

    public abstract Builder baseLayer(String value);

    public abstract Builder baseStyle(String value);

    public abstract Builder formats(String value);

    public abstract Builder cqlFilter(String value);

    public abstract Builder viewParams(String value);

    public abstract WMSConfiguration build();
  }
}
