package io.yogh.gwt.geo.command;

import io.yogh.gwt.geo.domain.IsLayer;
import io.yogh.gwt.geo.event.LayerChangeEvent.CHANGE;
import io.yogh.gwt.geo.event.LayerVisibleEvent;

public class LayerVisibleCommand extends LayerChangeCommand<LayerVisibleEvent> {
  public LayerVisibleCommand(final IsLayer<?> layer) {
    super(layer, CHANGE.VISIBLE);
  }

  @Override
  protected LayerVisibleEvent createEvent(final IsLayer<?> value) {
    return new LayerVisibleEvent(value);
  }
}
