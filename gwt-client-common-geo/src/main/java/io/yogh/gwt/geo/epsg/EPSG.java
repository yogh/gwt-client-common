package io.yogh.gwt.geo.epsg;

import com.google.inject.ImplementedBy;

import io.yogh.gwt.geo.BBox;

/**
 * EPSG map constants.
 */
@ImplementedBy(EPSGRDNew.class)
public abstract class EPSG {
  private static final String EPSG_PRE_TEXT = "EPSG:";

  private final int srid;
  private final BBox bounds;
  private final float maxResolution;
  private final String unit;
  private final int zoomLevel;

  /**
   * Construct a EPSG object.
   *
   * @param srid The SRID to set.
   * @param bounds The bounds to set.
   * @param resolutions The resolutions to set.
   * @param maxResolution The max resolution to set.
   * @param unit The unit to set.
   * @param zoomLevel The zoomLevel to set.
   */
  public EPSG(final int srid, final BBox bounds, final double[] resolutions, final float maxResolution, final String unit, final int zoomLevel) {
    this.srid = srid;
    this.bounds = bounds;
    this.maxResolution = maxResolution;
    this.unit = unit;
    this.zoomLevel = zoomLevel;
  }

  public String getEpsgCode() {
    return EPSG_PRE_TEXT + getSrid();
  }

  public int getSrid() {
    return srid;
  }

  public BBox getBounds() {
    return bounds;
  }

  public float getMaxResolution() {
    return maxResolution;
  }

  public String getUnit() {
    return unit;
  }

  public int getZoomLevel() {
    return zoomLevel;
  }

  @Override
  public String toString() {
    return "EPSG [srid=" + srid + ", bounds=" + bounds + ", maxResolution=" + maxResolution + ", unit=" + unit + ", zoomLevel=" + zoomLevel + "]";
  }
}
