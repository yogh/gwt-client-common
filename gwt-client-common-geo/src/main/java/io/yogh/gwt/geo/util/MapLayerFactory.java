package io.yogh.gwt.geo.util;

import com.google.web.bindery.event.shared.EventBus;

import io.yogh.gwt.geo.domain.IsLayer;
import io.yogh.gwt.geo.domain.layer.IsLayerConfiguration;

public interface MapLayerFactory<L> {
  IsLayer<L> prepareLayer(IsLayerConfiguration c, EventBus eventBus);
}
