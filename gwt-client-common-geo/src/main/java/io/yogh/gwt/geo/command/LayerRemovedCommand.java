package io.yogh.gwt.geo.command;

import io.yogh.gwt.geo.domain.IsLayer;
import io.yogh.gwt.geo.event.LayerChangeEvent.CHANGE;
import io.yogh.gwt.geo.event.LayerRemovedEvent;

public class LayerRemovedCommand extends LayerChangeCommand<LayerRemovedEvent> {
  public LayerRemovedCommand(final IsLayer<?> layer) {
    super(layer, CHANGE.ADDED);
  }

  @Override
  protected LayerRemovedEvent createEvent(final IsLayer<?> value) {
    return new LayerRemovedEvent(value);
  }
}
