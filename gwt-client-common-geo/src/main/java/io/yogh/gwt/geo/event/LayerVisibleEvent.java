package io.yogh.gwt.geo.event;

import io.yogh.gwt.geo.domain.IsLayer;

public class LayerVisibleEvent extends LayerChangeEvent {
  public LayerVisibleEvent(final IsLayer<?> layer) {
    super(layer, CHANGE.VISIBLE);
  }

}
