package io.yogh.gwt.geo.event;

import io.yogh.gwt.geo.domain.IsLayer;

public class LayerOpacityEvent extends LayerChangeEvent {
  private final double opacity;

  public LayerOpacityEvent(final IsLayer<?> layer, final double opacity) {
    super(layer, CHANGE.OPACITY);
    this.opacity = opacity;
  }

  public double getOpacity() {
    return opacity;
  }
}
