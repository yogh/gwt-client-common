package io.yogh.gwt.geo.domain;

import java.util.List;
import java.util.Optional;

import io.yogh.gwt.geo.domain.legend.LegendConfig;

public class LayerInfo {
  private String bundle;
  private String cluster;
  private String friendlyName;

  private String name;
  private String title;

  private LegendConfig legendConfig;

  private List<String> selectables;

  public LayerInfo() {}

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public LegendConfig getLegendConfig() {
    return legendConfig;
  }

  public void setLegendConfig(final LegendConfig legendConfig) {
    this.legendConfig = legendConfig;
  }

  public List<String> getSelectables() {
    return selectables;
  }

  public void setSelectables(final List<String> selectables) {
    this.selectables = selectables;
  }

  public boolean isBundle() {
    return bundle != null;
  }

  public String getBundle() {
    return bundle;
  }

  public void setBundle(final String bundle) {
    this.bundle = bundle;
  }

  public Optional<String> getCluster() {
    return Optional.ofNullable(cluster);
  }

  public void setCluster(final String cluster) {
    this.cluster = cluster;
  }

  public Optional<String> getFriendlyName() {
    return Optional.ofNullable(friendlyName);
  }

  public void setFriendlyName(final String friendlyName) {
    this.friendlyName = friendlyName;
  }
}
