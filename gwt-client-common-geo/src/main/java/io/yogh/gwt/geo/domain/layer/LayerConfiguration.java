package io.yogh.gwt.geo.domain.layer;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class LayerConfiguration {
  public static Builder builder() {
    return new AutoValue_LayerConfiguration.Builder()
        .visible(true)
        .opacity(1D);
  }

  public abstract String name();

  public abstract String title();

  public abstract boolean visible();

  public abstract double opacity();

  @AutoValue.Builder
  public static abstract class Builder {
    public abstract Builder name(String value);

    public abstract Builder title(String value);

    public abstract Builder visible(boolean value);

    public abstract Builder opacity(double value);

    public abstract LayerConfiguration build();
  }
}
